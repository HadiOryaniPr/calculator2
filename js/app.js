let showresult = document.getElementById("showresult");
let powerlog = false;

function calculator(value) {
  showresult.value += value;

  if (powerlog) {
    let number = showresult.value.split("^");
    showresult.value = Math.pow(number[0], number[1]);
    powerlog = false;
  }
}

function engineering(result) {
  if (result == "power") {
    powerlog = true;
    showresult.value += "^";
  } else {
    showresult.value = Math[result](showresult.value);
  }
}

function reset() {
  showresult.value = null;
}

function equal() {
  showresult.value = eval(showresult.value);
}
